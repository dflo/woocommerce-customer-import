<?php
/**
 * Adds, validates and saves the email field on the add-to-cart page.
 *
 * @package Woocommerce_Customer_Import
 * @since 1.0.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * This class adds, validates and saves the email field on the add-to-cart page.
 *
 * @since      1.0.0
 * @package    Woocommerce_Customer_Import
 * @subpackage Woocommerce_Customer_Import/includes
 */
class Woocommerce_Customer_Import {
	/**
	 * The single instance of the class
	 *
	 * @var Woocommerce_Customer_Import
	 * @since 1.0.0
	 */
	protected static $_instance = null;

	/**
	 * Main Woocommerce_Customer_Import Instance.
	 *
	 * Ensures only one instance of Woocommerce_Customer_Import is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @return Woocommerce_Customer_Import Main instance
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Initialize the class.
	 */
	public function __construct() {
		if ( defined( 'WOOCOMMERCE_CUSTOMER_IMPORT_VERSION' ) ) {
			$this->version = WOOCOMMERCE_CUSTOMER_IMPORT_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'woocommerce-customer-import';

		$this->run();
	}

	/**
	 * Runs the execution of the plugin.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->load_dependencies();
		WCI_Import_Customers::instance();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * The admin files.
		 */
		require_once WCI_PLUGIN_PATH . 'admin/class-wci-import-customers.php';
	}
}
