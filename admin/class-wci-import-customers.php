<?php
/**
 * Admin page to import Woocommerce Customers.
 *
 * @package WCI_Import_Customers
 * @since 1.0.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * This class adds the admin page to import Woocommerce Customers.
 *
 * @since      1.0.0
 * @package    Woocommerce_Customer_Import
 * @subpackage Woocommerce_Customer_Import/admin
 */
class WCI_Import_Customers {
	/**
	 * The single instance of the class.
	 *
	 * @var WCI_Import_Customers
	 * @access   protected
	 * @since 1.0.0
	 */
	protected static $instance = null;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		add_action( 'admin_menu', array( $this, 'menu_page' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'wp_ajax_wci_upload_csv', array( $this, 'upload_csv' ) );
	}

	/**
	 * Main WCI_Import_Customers instance. Ensures only one instance of WCI_Import_Customers is loaded or can be loaded.
	 *
	 * @static
	 * @return WCI_Import_Customers
	 * @since  1.0.0
	 */
	public static function instance() {
		if ( is_null( self::$instance ) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Register the menu for Ebridge Sync
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function menu_page() {
		add_menu_page( __( 'Import Customers', 'woocommerce-customer-import' ), __( 'Import Customers', 'woocommerce-customer-import' ), 'manage_options', 'wci_import_customers', array( $this, 'render_settings_page' ) );
		add_submenu_page( 'wci_import_customers', esc_html__( 'Import Customers', 'woocommerce-customer-import' ), esc_html__( 'Import Customers', 'woocommerce-customer-import' ), 'manage_options', 'wci_import_customers', array( &$this, 'render_settings_page' ) );
		remove_submenu_page( 'wci_import_customers', 'wci_import_customers' );
	}

	/**
	 * Render the Ebridge Sync settings page
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function render_settings_page() {
		$tabs = array(
			'import_customers' => __( 'Import Customers', 'woocommerce-customer-import' ),
		);

		$tab = isset( $_GET['tab'] ) ? sanitize_text_field( $_GET['tab'] ) : 'import_customers';

		?>
		<!-- Create a header in the default WordPress 'wrap' container -->
		<div class="wci-wrap">
			<h2><?php esc_html_e( '', 'woocommerce-customer-import' ); ?></h2>
			<h2 class="nav-tab-wrapper">
		<?php
		foreach ( $tabs as $key => $value ) {
			$active = ( $key == $tab ) ? 'nav-tab-active' : '';
			echo '<a class="nav-tab ' . $active . '" href="?page=wci_import_customers&tab=' . esc_attr( $key ) . '">' . esc_html( $value ) . '</a>';
		}
		?>
			</h2>
			<?php
			switch ( $tab ) {
				case 'import_customers':
					$this->import_customers();
					break;
				default:
					$this->import_customers();
					break;
			}
			?>
		</div>
			<?php
	}

	/**
	 * Section to display the tab to import Customers.
	 *
	 * @since    1.0.0
	 */
	public function import_customers() {
		?>
			<h2><?php esc_html_e( 'Import Customers', 'woocommerce-customer-import' ); ?></h2>
			<p><?php esc_html_e( 'Add a .csv file. The first row is reserved for headings.', 'woocommerce-customer-import' ); ?></p>
			<p><?php esc_html_e( 'Please click', 'woocommerce-customer-import' ); ?>
				<a href="<?php echo esc_url( WCI_PLUGIN_URL . 'data/example.csv' ); ?>"><?php esc_html_e( 'here', 'woocommerce-customer-import' ); ?></a>
			<?php esc_html_e( 'to download the reference file.', 'woocommerce-customer-import' ); ?>
			</p>

			<form id="wci_form" action="#" method="post" enctype="multipart/form-data">
				<div class="import_button">
					<input type="file" name="wci_csv" id="wci_csv" class="file" accept=".csv" data-show-preview="false" data-show-upload="false" title="<?php esc_html_e( 'Select File', 'woocommerce-customer-import' ); ?>">
				<?php wp_nonce_field( 'filesystem-nonce', 'cust-nonce' ); ?>
					<input type="submit" id="wci_submit" name="wci_submit" class="button button-primary" value="<?php esc_html_e( 'Import', 'woocommerce-customer-import' ); ?>">
				</div>
			</form>
		<?php
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		wp_register_script( 'js-validator', WCI_PLUGIN_URL . 'admin/assets/js/jquery.validate.min.js', array( 'jquery' ), '1.19.3', true );

		wp_register_script( 'wci-admin', WCI_PLUGIN_URL . 'admin/assets/js/wci-import-customers.js', array( 'jquery' ), WOOCOMMERCE_CUSTOMER_IMPORT_VERSION, true );

		if ( ( isset( $_GET['page'] ) ) && ( 'wci_import_customers' === $_GET['page'] ) ) {
			wp_enqueue_script( 'js-validator' );
			wp_enqueue_script( 'wci-admin' );
			wp_localize_script( 'wci-admin', 'wci', $this->add_localize_script() );
		}
	}

	/**
	 * Adds neede js variables.
	 *
	 * @since    1.0.0
	 */
	public function add_localize_script() {
		$args = array(
			'admin_url' => admin_url( 'admin-ajax.php' ),
		);

		return $args;
	}

	/**
	 * Ajax to upload data from csv and update user records.
	 *
	 * @since 1.0.0
	 */
	public function upload_csv() {
		$response = array();

		$file_nonce = wc_get_var( $_POST['cust-nonce'], '' ); // @codingStandardsIgnoreLine.

		if ( ! wp_verify_nonce( $file_nonce, 'filesystem-nonce' ) ) {
			$response['message'] = __( 'Unable to upload file. Authentication failed.', 'woocommerce-customer-import' );
			wp_send_json_error( $response );
		}

		if ( isset( $_FILES['wci_csv'] ) && ! empty( $_FILES['wci_csv'] ) ) {
			$customer_data = $this->get_file_data( $_FILES['wci_csv'] ); // @codingStandardsIgnoreLine.
			$customer_data = $this->santize_customer_data( $customer_data );

			$response            = $this->upload_customer_data( $customer_data );
			$response['message'] = __( 'The number of Customers successfully uploaded: ', 'woocommerce-customer-import' ) . count( $response );
		}

		wp_send_json_success( $response );
	}

	/**
	 * Validates and returns the file data in an array.
	 *
	 * @since 1.0.0
	 * @param array $file The unformatted file data.
	 *
	 * @return array File data.
	 */
	public function get_file_data( $file ) {
		$file_data = array();

		$cust_csv = array(
			'name'     => $file['name'],
			'type'     => $file['type'],
			'tmp_name' => $file['tmp_name'],
			'error'    => $file['error'],
			'size'     => $file['size'],
		);

		$attachment_path = $this->upload_attachment( $cust_csv );

		$row    = 1;
		$keys   = array();
		$handle = fopen( $attachment_path, 'r' );

		if ( $handle ) {
			$data = fgetcsv( $handle, 1000, ',' );

			while ( $data ) {
				if ( 1 === $row ) {
					$keys = $data;
					$row++;
					$data = fgetcsv( $handle, 1000, ',' );
					continue;
				}

				$no_of_cols    = count( $data );
				$data_key_vals = array();
				for ( $i = 0; $i < $no_of_cols; $i++ ) {
					$data_key_vals[ $keys[ $i ] ] = $data[ $i ];
				}
				$file_data[] = $data_key_vals;
				$row++;

				$data = fgetcsv( $handle, 1000, ',' );
			}

			fclose( $handle );
		}

		wp_delete_file( $attachment_path );

		return $file_data;
	}

	/**
	 * Upload attachment to uploads folder.
	 *
	 * @since 1.0.0
	 * @param string $file_to_upload file path.
	 *
	 * @return string|null Uploaded file or null.
	 */
	public function upload_attachment( $file_to_upload ) {
		if ( ! function_exists( 'wp_handle_upload' ) ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';
		}

		$upload_overrides = array(
			'test_form' => false,
		);

		$movefile = wp_handle_upload( $file_to_upload, $upload_overrides );

		if ( $movefile && ! isset( $movefile['error'] ) ) {
			return $movefile['file'];
		} else {
			return null;
		}
	}

	/**
	 * Upload data as Woocommerce customers.
	 *
	 * @since 1.0.0
	 * @param array $customer_data Customer data.
	 *
	 * @return array Upload information.
	 */
	public function upload_customer_data( $customer_data ) {
		$upload_data = array();

		foreach ( $customer_data as $row => $customer ) {
			$defaults = array(
				'email'              => '',
				'first_name'         => '',
				'last_name'          => '',
				'display_name'       => '',
				'role'               => 'customer',
				'username'           => '',
				'billing'            => array(
					'first_name' => '',
					'last_name'  => '',
					'company'    => '',
					'address_1'  => '',
					'address_2'  => '',
					'city'       => '',
					'postcode'   => '',
					'country'    => '',
					'state'      => '',
					'email'      => '',
					'phone'      => '',
				),
				'shipping'           => array(
					'first_name' => '',
					'last_name'  => '',
					'company'    => '',
					'address_1'  => '',
					'address_2'  => '',
					'city'       => '',
					'postcode'   => '',
					'country'    => '',
					'state'      => '',
				),
				'is_paying_customer' => false,
			);

			$customer = wp_parse_args( $customer, $defaults );
			$uploaded = $this->sync_customer_data( $customer );

			$upload_data[] = array(
				'customer_data' => $customer,
				'status'        => $uploaded['success'],
				'result'        => $uploaded,
			);
		}

		return $upload_data;
	}

	/**
	 * Sanitize data received from csv file.
	 *
	 * @since 1.0.0
	 * @param array $customer_data Customer data.
	 *
	 * @return array Sanitized Customer data.
	 */
	public function santize_customer_data( $customer_data ) {
		$data = array();

		foreach ( $customer_data as $row => $customer ) {
			$data[] = array(
				'email'              => isset( $customer['email'] ) && is_email( sanitize_email( $customer['email'] ) ) ? sanitize_email( $customer['email'] ) : '',
				'first_name'         => isset( $customer['first_name'] ) ? sanitize_text_field( $customer['first_name'] ) : '',
				'last_name'          => isset( $customer['last_name'] ) ? sanitize_text_field( $customer['last_name'] ) : '',
				'display_name'       => isset( $customer['display_name'] ) ? sanitize_text_field( $customer['display_name'] ) : '',
				'username'           => isset( $customer['username'] ) ? sanitize_text_field( $customer['username'] ) : '',
				'billing'            => array(
					'first_name' => isset( $customer['billing_first_name'] ) ? sanitize_text_field( $customer['billing_first_name'] ) : '',
					'last_name'  => isset( $customer['billing_last_name'] ) ? sanitize_text_field( $customer['billing_last_name'] ) : '',
					'company'    => isset( $customer['billing_company'] ) ? sanitize_text_field( $customer['billing_company'] ) : '',
					'address_1'  => isset( $customer['billing_address_1'] ) ? sanitize_text_field( $customer['billing_address_1'] ) : '',
					'address_2'  => isset( $customer['billing_address_2'] ) ? sanitize_text_field( $customer['billing_address_2'] ) : '',
					'city'       => isset( $customer['billing_city'] ) ? sanitize_text_field( $customer['billing_city'] ) : '',
					'postcode'   => isset( $customer['billing_postcode'] ) ? sanitize_text_field( $customer['billing_postcode'] ) : '',
					'country'    => isset( $customer['billing_country'] ) ? sanitize_text_field( $customer['billing_country'] ) : '',
					'state'      => isset( $customer['billing_state'] ) ? sanitize_text_field( $customer['billing_state'] ) : '',
					'email'      => isset( $customer['billing_email'] ) ? sanitize_text_field( $customer['billing_email'] ) : '',
					'phone'      => isset( $customer['billing_phone'] ) ? sanitize_text_field( $customer['billing_phone'] ) : '',
				),
				'shipping'           => array(
					'first_name' => isset( $customer['shipping_first_name'] ) ? sanitize_text_field( $customer['shipping_first_name'] ) : '',
					'last_name'  => isset( $customer['shipping_last_name'] ) ? sanitize_text_field( $customer['shipping_last_name'] ) : '',
					'company'    => isset( $customer['shipping_company'] ) ? sanitize_text_field( $customer['shipping_company'] ) : '',
					'address_1'  => isset( $customer['shipping_address_1'] ) ? sanitize_text_field( $customer['shipping_address_1'] ) : '',
					'address_2'  => isset( $customer['shipping_address_2'] ) ? sanitize_text_field( $customer['shipping_address_2'] ) : '',
					'city'       => isset( $customer['shipping_city'] ) ? sanitize_text_field( $customer['shipping_city'] ) : '',
					'postcode'   => isset( $customer['shipping_postcode'] ) ? sanitize_text_field( $customer['shipping_postcode'] ) : '',
					'country'    => isset( $customer['shipping_country'] ) ? sanitize_text_field( $customer['shipping_country'] ) : '',
					'state'      => isset( $customer['shipping_state'] ) ? sanitize_text_field( $customer['shipping_state'] ) : '',
				),
				'is_paying_customer' => isset( $customer['is_paying_customer'] ) ? ( strtolower( sanitize_text_field( $customer['is_paying_customer'] ) ) === 'yes' ? true : false ) : '',
			);
		}

		return $data;
	}

	/**
	 * Sync customer data. Create cusomer if not present. Otherwise update.
	 *
	 * @param array $customer_data Customer Data.
	 * @return object|WP_Error|bool
	 */
	public function sync_customer_data( $customer_data ) {
		$updated = array();

		$user = get_user_by( 'email', $customer_data['email'] );

		if ( ! $user ) {
			$customer_id = wc_create_new_customer( $customer_data['email'], '', '', $customer_data );

			if ( is_wp_error( $customer_id ) ) {
				if ( 'registration-error-email-exists' == $customer_id->get_error_code() ) {
					$user        = get_user_by( 'email', $customer_data['email'] );
					$customer_id = $user->ID;
				} else {
					$updated['success']       = false;
					$updated['error_code']    = $customer_id->get_error_code();
					$updated['error_message'] = $customer_id->get_error_message();

					return $updated;
				}
			}
		} else {
			$customer_id = $user->ID;
		}

		$updated['success']     = true;
		$updated['customer_id'] = $customer_id;

		self::update_customer_data( $customer_id, $customer_data );

		return $updated;
	}

	/**
	 * Update User data .
	 *
	 * @static
	 * @param object $customer_id User ID.
	 * @param object $customer_data Customer Data.
	 * @return void
	 */
	public static function update_customer_data( $customer_id, $customer_data ) {
		$customer = new WC_Customer( $customer_id );

		if ( isset( $customer_data['email'] ) && ! empty( $customer_data['email'] ) ) {
			$customer->set_email( $customer_data['email'] );
		}

		if ( isset( $customer_data['first_name'] ) && ! empty( $customer_data['first_name'] ) ) {
			$customer->set_first_name( $customer_data['first_name'] );
		}

		if ( isset( $customer_data['last_name'] ) && ! empty( $customer_data['last_name'] ) ) {
			$customer->set_last_name( $customer_data['last_name'] );
		}

		if ( isset( $customer_data['billing'] ) && ! empty( $customer_data['billing'] ) ) {
			$customer_billing = $customer_data['billing'];

			$customer->set_billing_first_name( $customer_billing['first_name'] );
			$customer->set_billing_last_name( $customer_billing['last_name'] );

			$customer->set_billing_address( $customer_billing['address1'] );
			$customer->set_billing_address_2( $customer_billing['address2'] );
			$customer->set_billing_city( $customer_billing['city'] );
			$customer->set_billing_state( $customer_billing['state'] );
			$customer->set_billing_postcode( $customer_billing['post_code'] );
		}

		if ( isset( $customer_data['shipping'] ) && ! empty( $customer_data['shipping'] ) ) {
			$customer_shipping = $customer_data['shipping'];

			$customer->set_shipping_first_name( $customer_shipping['first_name'] );
			$customer->set_shipping_last_name( $customer_shipping['last_name'] );

			$customer->set_shipping_address( $customer_shipping['address1'] );
			$customer->set_shipping_address_2( $customer_shipping['address2'] );
			$customer->set_shipping_city( $customer_shipping['city'] );
			$customer->set_shipping_state( $customer_shipping['state'] );
			$customer->set_shipping_postcode( $customer_shipping['post_code'] );
		}

		$customer->save();
	}
}
