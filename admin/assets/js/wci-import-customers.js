(function( $ ) {
	'use strict';

	$( window ).load(
		function() {
			$( '#wci_form' ).validate(
				{
					rules: {
						wci_csv: {
							required: true,
						}
					},
					messages: {
						wci_csv: {
							required: 'Please select a valid csv file.',
						}
					},
					submitHandler: function (form) {
						event.preventDefault();
						$( '.import_button' ).prepend( '<div class="wci-loader-container"><div class="wci-loader"></div></div>' );
						$( "#message-wrap" ).remove();
						var formData = new FormData( document.getElementById( 'wci_form' ) );
						formData.append( "action", "wci_upload_csv" );
						$.ajax(
							{
								url: wci.admin_url,
								type: 'post',
								dataType: 'json',
								data:  formData,
								contentType: false,
								cache: false,
								processData: false,
								success: function (response) {
									$( '.wci-loader-container' ).remove();
									if (response.success) {
										$( '<div id="message-wrap"><h3>Logs:</h3><p id="message">' + response.data.message + '</p></div>' ).insertAfter( '#wci_form' );
									} else {
										$( '<div id="message-wrap"><h3>Logs:</h3><p id="message">' + response.data.message + '</p></div>' ).insertAfter( '#wci_form' );
									}
								}
							}
						);
					}
				}
			);
		}
	);

})( jQuery );
