<?php
/**
 * Plugin Name: WooCommerce Customer Import
 * Description: A Woocommerce add-on to import users as Woocommerce Customers.
 * Version: 1.0.0
 * Author: Flora Dsouza
 * Author URI: https://profiles.wordpress.org/floradsouza/
 * Text Domain: woocommerce-customer-import
 * Domain Path: /i18n/languages/
 *
 * @package WooCommerce_Customer_Import
 */

defined( 'ABSPATH' ) || exit;

// Define WCI_PLUGIN_PATH.
if ( ! defined( 'WCI_PLUGIN_PATH' ) ) {
	define( 'WCI_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
}

// Define WCI_PLUGIN_URL.
if ( ! defined( 'WCI_PLUGIN_URL' ) ) {
	define( 'WCI_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}

// Define WOOCOMMERCE_CUSTOMER_IMPORT_VERSION.
if ( ! defined( 'WOOCOMMERCE_CUSTOMER_IMPORT_VERSION' ) ) {
	define( 'WOOCOMMERCE_CUSTOMER_IMPORT_VERSION', '1.0.0' );
}

// function wci_check_woocommerce_active() {
// if ( wci_check_woocommerce_plugin_status() ) {
// Woocommerce_Customer_Import::run();
// } else {
// Deactivate the plugin.
// deactivate_plugins( __FILE__ );
// $error_message = __( 'WooCommerce Customer Import plugin requires WooCommerce plugin to be active!', 'woocommerce-customer-import' );
// 		wp_die( $error_message ); // @codingStandardsIgnoreLine.
// 	}
// }

// function wci_check_woocommerce_plugin_status() {
// it the plugin is active, we're good.
// if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
// return true;
// }

// if ( ! is_multisite() ) {
// return false;
// }

// $plugins = get_site_option( 'active_sitewide_plugins' );

// return isset( $plugins['woocommerce/woocommerce.php'] );
// }

// add_action( 'plugins_loaded', 'wci_check_woocommerce_active' );

require_once WCI_PLUGIN_PATH . 'includes/class-woocommerce-customer-import.php';
Woocommerce_Customer_Import::instance();
